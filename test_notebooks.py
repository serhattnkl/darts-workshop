import os

import nbformat
from nbconvert.preprocessors import ExecutePreprocessor, CellExecutionError

def run_notebook(notebook_fname):
    '''
    run notebook from given filename
    return 0 if success, 1 if failed
    '''
    notebook_fname_out = os.path.join('out', notebook_fname)

    with open(notebook_fname) as f:
        nb = nbformat.read(f, as_version=4)

    # initialize python3 notebook processor with timeout 5 min
    ep = ExecutePreprocessor(timeout=300, kernel_name='python3')

    # run the notebook file
    try:
        ep.preprocess(nb, {'metadata': {'path': '.'}})
    except CellExecutionError:
        msg = 'Error executing the notebook "%s".\n' % notebook_fname
        msg += 'See notebook "%s" for the traceback.' % notebook_fname_out
        print(msg)
        return 1
    finally:
        # write result notebook
        with open(notebook_fname_out, mode='w', encoding='utf-8') as f:
            nbformat.write(nb, f)
    return 0


def run_all():
    '''
    runs all ipynb files in the current directory
    returns the number of failed notebooks (0 if all succeed)
    '''
    if not os.path.exists('out'):
        os.mkdir('out')

    n_total = n_failed = 0

    for filename in os.listdir():
        if filename.endswith(".ipynb"):
            rcode = run_notebook(filename)
            n_failed += rcode
            n_total += 1

    print('PASSED', n_total - n_failed, 'of', n_total)
    return n_failed

if __name__ == '__main__':
    run_all()
